# PAM SERVER 2022 INTERACTIVO
# Isaac Gordillo

## **Práctica include & substack**

El objetivo de la práctica es demostrar la diferencia que hay entre la manera de **controlar** un servicio según la opción **include** o **substack**.

Lo que hace la primera es incluir todas las líneas de un tipo de servicio extraídas del fichero de configuración indicado como argumento del control.

La segunda hace lo mismo pero tratando al archivo que contiene las líneas por separado para que, de esta forma, un apartado del montón (stack) de normas no afecte a su totalidad y este pueda seguir ejecutándose.

Por ejemplo, se puede hacer que el servicio **/etc/pam.d/chfn** se rija solo por dos conjuntos de normas: las que se definen en el propio fichero y las definidas en el archivo de configuración **prueba_chfn**, guardado en el mismo directorio.

### Ejemplo con include

- **/etc/pam.d/chfn**:

  ```
  auth    include       prueba_chfn
  auth    optional      pam_echo.so "Adios"
  ```

- **/etc/pam.d/prueba_chfn**:

  ```
  auth    optional	pam_echo.so "Hola"                           # Es muy importante que
  auth    required	pam_unix.so                                  # "uid > 1000" esté escrito           
  auth    requisite	pam_succeed_if.so debug uid > 1000           # exactamente de esa forma.
  auth    optional	pam_echo.so "Prueba realizada"
  ```

  * Al ejecutar el servicio **chfn** con la opción **include** ocurre lo siguiente:

    * Se ejecutan las líneas del archivo **prueba_chfn**:
  
      * Primero se muestra un mensaje diciendo "Hola".
      * Luego se comprueba si el usuario es de tipo unix, pero al tener la opción **required** se seguirán ejecutando los siguientes módulos del archivo sin importar el resultado obtenido hasta la ejecución de esta línea.
      * A continuación se comprueba si el UID del usuario mayor que 1000.
      * **Solo si el UID del usuario que ejecuta el servicio es mayor 1000** se muestra el mensaje "Prueba realizada" .
      * Finalmente se muestra el mensaje "Adios".
    
    * Una vez las líneas de este fichero han sido ejecutadas, **solo si el resultado es positivo** se muestra el mensaje "Adios".

  * Posibles resultados:

    * Si el usuario es de tipo unix y tiene un UID > 1000, el mensaje de "Prueba realizada" y el de "Adios" se mostrarán por pantalla y se terminará dando permiso al usuario para ejecutar el servicio.

    * Si el usuario es de tipo unix pero tiene un UID <= 1000, el mensaje de "Prueba realizada" se mostrará pero no el de "Adios" y no tendrá permiso para ejecutar el servicio.

    * Si el usuario no es de tipo unix pero tiene un UID > 1000, el mensaje de "Prueba realizada" se mostrará, aunque el resultado acumulado será negativo, cosa que hará que se muestre el mensaje de "Adios" pero que el usuario no tenga permiso para ejecutar el servicio.

    * Si el usuario no es de tipo unix y tiene un UID <= 1000, el mensaje de "Prueba realizada" no se mostrará y no tendrá permiso para ejecutar el archivo.

De esta forma, con el control **include** lo que se consigue es hacer que si el resultado de las acciones del fichero **prueba_chfn** dan como resultado la ejecución de las acciones **done** o **die**, todas las demás normas del archivo de configuración del servicio chfn den el mismo resultado.

### Ejemplo con substack

- **/etc/pam.d/chfn**:

  ```
  auth    substack      prueba_chfn
  auth    optional      pam_echo.so "Adios"
  ```

- **/etc/pam.d/prueba_chfn**:

  ```
  auth    optional	pam_echo.so "Hola"
  auth    required	pam_unix.so
  auth    requisite	pam_succeed_if.so debug uid > 1000
  auth    optional	pam_echo.so "Prueba realizada"
  ```

  * La diferencia que hay al ejecutar el servicio **chfn** con la opción **substack** es que el mensaje de "Adios" siempre se muestra por pantalla, por lo que siempre se continúa con la ejecución de los demás módulos de su archivo de configuración.

    * Se otorga permiso de ejecución del servicio en función de las condiciones cumplidas en el substack.