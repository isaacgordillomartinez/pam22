# PAM SERVER 2022 INTERACTIVO
# Isaac Gordillo


Este es un contenedor de Docker para practicar con PAM.

## EJECUCIÓN DEL CONTENEDOR

```
docker build -t isaacgm22/pam22:base .
```

```
docker run --rm --name pam.edt.org -h pam.edt.org --privileged --network 2hisx -it isaacgm22/pam22:base
```

NOTA: **--privileged** da privilegios extendidos al contenedor. A un contenedor "privilegiado" se le da acceso a todos los dispositivos.

Aquí no se indica /bin/bash ya que está indicado en el startup.sh. De hecho, si se indica pueden no ejecutarse algunas órdenes del script, como por ejemplo las de crear cuentas de usuarios con sus contraseñas.

## STARTUP.SH

```
#! /bin/bash

for user in unix01 unix02 unix03 unix04 unix05
do
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | passwd $user
done

/bin/bash
```

## TIPOS DE ARCHIVOS DE DEFINICIÓN DE REGLAS

Se encuentran en /etc/pam.d

- Reglas comunes:

    * Son aquellos que tienen de nombre "common-*" y que pueden o no ser
      incluidos en archivos de reglas específicas.

      ```
      common-account
      common-auth
      common-password
      common-session
      common-session-noninteractive
      ```
    
      * Ejemplo de common-password:

        ```
        # here are the per-package modules (the "Primary" block)
        password    requisite                   pam_pwquality.so retry=3
        password    [success=1 default=ignore]  pam_unix.so obscure use_authok try_first_pass yescrypt
        # here's the fallback if no module succeeds
        password    requisite                   pam_deny.so
        # prime the stack with a positive return value if there isn't one already;
        # this avoid us returning an error just because nothin sets a success code
        # since the modules above will each just jump around
        password    required                    pam_permit.so
        # and here are more per-package modules (the "Additional" block)
        password    optional                    pam_mount.so disable_interactive
        # end of pam-auth-update config
        ```

- Reglas específicas:

  * Son aquellos que sirven para órdenes individuales y que pueden heredar
    reglas comunes.

    ```
    chfn
    chpassword
    chsh
    login
    newusers
    passwd
    runuser
    runuser-l
    su
    su-l
    ```

    * Ejemplo de chfn:

      ```
      #
      # The PAM configuration file for the Shadow 'chfn' service
      #

      # This allows root to change user information without being
      # prompted for a password
      auth      sufficient    pam_rootok.so
      # The standard Unix authentication modules, used with
      # NIS (man nsswitch) as well as normal /etc/paswd and
      # /etc/shadow entries.
      @include common-auth              # Así se indica de qué archivos
      @include common-account           # se heredan reglas.
      @include common-session
      ```

- Otros:

  * El archivo **other** sirve para crear definir órdenes que no tienen
    archivos individuales. También puede heredar reglas comunes.

    * Ejemplo de other:

      ```
      #
      # /etc/pam.d/other - specify the PAM fallback behaviour
      # Note that this file is used for any unspecified service; for example
      # if /etc/pam.d/cron specifies no session modules but cron calls
      # pam_open_session, the session module out of /etc/pam.d/other is
      # used. If you really want nothing to happen then use pam_permit.so or
      # pam_deny.so as appropiate.

      # We fall back to the system default in /etc/pam.d/common-*
      #

      @include common-auth
      @include common-account
      @include common-password
      @include common-session
      ```


## EJEMPLOS DE CAMBIO DE FINGERNAME

SINTAXIS:

```
type control module-path module-arguments
```

Dentro del contendedor, en **/etc/pam.d/chfn**.

```
#
# The PAM configuration file for the Shadow 'chfn' service
#
# PAM ejemplo-01

auth        sufficient      pam_rootok.so

auth        optional        pam_echo.so "hola"
auth        sufficient      pam_deny.so

account     optional        pam_echo.so "adeu"
account     sufficient      pam_deny.so
```

### EJEMPLO 1

El usuario tiene permiso para cambiar de todo.

**man pam_permit**:

```
type         control         module

account      required        pam_permit.so
```

**getent passwd user**:

unix01:x:1000:1000:,bb,bb,bb:/home/unix01:/bin/bash

### EJEMPLO 2

El usuario NO tiene permisos.

**man pam_deny**:

```
type         control         module
```

```
auth    optional    pam_echo.so [ missatges user %u ]
auth    optional    pam_echo.so [ host %h service % ]
auth    sufficient  pam_deny.so
account sufficient  pam_deny.so
```

### EJEMPLO 3

El usuario tiene permisos según se autentique.

**man pam_unix**:

```
nullok
           The default action of this module is to not permit the user access to a service if their
           official password is blank. The nullok argument overrides this default.
```

### EJEMPLO PARA GUARDAR

```
auth	optional	pam_echo.so [ missatges user %u ]
auth	optional	pam_echo.so [ host %h service % ]
# auth	sufficient	pam_deny.so

auth	required	pam_permit.so
auth 	required	pam_unix.so
auth	required	pam_succeed_if.so debug uid >= 1000
auth	required	pam_succeed_if.so user = unix01

account	sufficient	pam_permit.so
```

## COMMON-PASSWORD

Este archivo está en **/etc/pam.d**. Las normas que tenga definidas pueden ser configuradas también usando el archivo **/etc/security/pwquality.conf**.

### EJEMPLO 1

Los usuarios no pueden cambiar su contraseña.

```
password  requisite     pam_pwquality.so retry=3
password  required      pam_unix.so
password  requisite     pam_deny
```

### EJEMPLO 2

Los usuarios sí que pueden cambiar su contraseña.

```
password  requisite     pam_pwquality.so retry=3
password  sufficient    pam_unix.so            # Aquí está el cambio.
password  requisite     pam_deny
```

### EJEMPLO 3

Los usuarios pueden cambiar su contraseña, pero tienen 3 intentos y esta ha de tener, como mínimo, 1 carácter.

```
password  requisite     pam_pwquality.so retry=3 minlen=1
password  required      pam_unix.so
password  requisite     pam_deny
```

## PRÁCTICA 1

**IMPORTANTE**: el módulo **pam_time.so** solo funciona con el type **account**.

Hacer que los usuarios puedan cambiar su **fingername**, pero solo en un cierto intervalo de tiempo.

- Para conseguirlo primero hay que modificar el archivo **/etc/pam.d/chfn** añadiendo la siguiente línea:

  ```
  account   required    pam_time.so
  ```

- Después hay que modificar el archivo **/etc/security/time.conf**. Su sintaxis es la siguiente:

  ```
  services;ttys;users;times
  ```

  * Lo que estas líneas expresan es que se puede usar el servicio chfn en todos los terminales, que esto se aplica a los usuarios unix01 y unix02, y que se hace todos los días las 24 horas del día (00:00h - 24:00h).

    ```
    chfn;*;unix01&unix02;Al0000-2400
    ```

Con esta configuración base los usuarios pueden usar chfn con éxito siempre que se validen según lo indicado en **/etc/pam.d/common-password** y **/etc/security/pwquality.conf**.

### EJERCICIO 1

Todos los usuarios pueden usar el servicio chfn de 8:00h a 10:00h.

- /**etc/security/time.conf**:

**IMPORTANTE**: Indicar "Al0800-1000" implica que el tiempo de uso es de 08:00h a 10:00h, solo que el container va una hora atrasado.

  ```
  chfn;*;*;Al0800-1000
  ```

### EJERCICIO 2

Ningún usuario puede usar el servicio chfn de 8:00h a 10:00h

  ```
  chfn;*;*;!Al0800-1000
  ```

### EJERCICIO 3

Todos los usuarios usar el servicio chfn de 8:00h a 10:00h y de 16:00h a 22:00h.

  ```
  chfn;*;*;Al0800-0900|Al1600-2200      # Aquí es importante usar un OR (|) y no un
                                          AND (& o escribir dos líneas).
  ```

### EJERCICIO 4

El usuario unix01 puede usar el servicio chfn de 8:00h a 14:00h. Los otros usuarios no pueden.

  ```
  chfn;*;!unix01;!Al0800-1400
  ```

### EJERCICIO 5

El usuario unix01 solo puede conectarse los días laborables/entre semana (working days).

- Para esto hay que modificar el archivo **/etc/pam.d/login**

  ```
  login;*;unix01;Wk0000-2400
  ```

### EJERCICIO 6

El usuario unix02 se puede conectar todos los días pero solo por la tarde.

  ```
  login;*;unix02;Al1200-2400
  ```