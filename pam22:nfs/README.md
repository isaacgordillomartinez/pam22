# PAM SERVER 2022 INTERACTIVO
# Isaac Gordillo


## **Ejecución del contenedor**

```
docker build -t isaacgm22/pam22:nfs .
```

```
docker run --rm --name pam.edt.org -h pam.edt.org --privileged --network 2hisx -it isaacgm22/pam22:nfs
```

### Archivos que no cambian:

- startup.sh

## **Cambios respecto al contenedor base**

### Dockerfile

Se ha añadido a la orden de instalación las utilidades típicas de cliente del servicio NFS (**nfs-common**):

```
RUN apt-get update && apt-get -y install procps tree nmap vim less finger passwd iproute2 iputils-ping libpam-pwquality libpam-mount nfs-common
```

Para automatizar los procesos de prueba de este documento se sustituirá el archivo **common-session** modificado, que está incluido en la imagen del contenedor, por el que viene defecto dentro de este en el directorio **/etc/pam.d**.

```
COPY common-session /etc/pam.d/
```

Lo mismo se hace con el archivo **pam_mount.conf.xml** sustituyéndolo por el que viene por defecto dentro del contenedor en el directorio **/etc/security**.

```
COPY pam_mount.conf.xml /etc/security/
```

## **Teoría**

Este es un contenedor de Docker para practicar con PAM el servicio NFS.

Para ello hace falta usar los siguientes módulos:

- **pam_unix.so** para poder iniciar sesión como usuario.
- **pam_mkhomedir.so** para crear un homedir a cada usuario al iniciar sesión.
- **pam_mount.so** para montar los homedir.

## pam_mkhomedir

Este módulo solo funciona con el tipo **session** y sirve para crear directorios home de usuarios cuando estos inician sesión.

El archivo de definición de reglas específicas de inicio de sesión es **/etc/pam.d/login**, que hereda reglas comunes del archivo **/etc/pam.d/common-session**, por lo que será en este último en el que habrá que hacer cambios.

Como este archivo se procesa línea a línea es importante el orden en el que están escritas las instrucciones de ejecución de módulos, por lo que primero se inicia sesión **(unix)**, luego se crea el homedir del usuario que acaba de iniciar sesión **(mkhomedir)** y luego este se monta **(mount)**.

```
session required	pam_unix.so
session	required	pam_mkhomedir.so
session	optional	pam_mount.so
```

## pam_mount

En la mayoría de las plataformas este módulo se configura mediante el archivo **/etc/security/pam_mount.conf.xml** (si no, está en /etc).

En este archivo se pueden cambiar, entre otros factores, el tipo de sistema de ficheros que se usará (**fstype="tmpfs"**, por ejemplo), la localización del volumen a montar del servidor (**path="/path/to/dir"**) y el directorio destino donde quedará montado lo seleccionado anteriormente (**mountpoint="/route/to/dir"**).

## NFS

Montar de forma local lo que tiene la ruta indicada del localhost en /mnt.

```
sudo mount -t nfs localhost:/usr/share/doc /mnt
```

Montar todo aquello que sea de tipo nfs4.

```
sudo mount -t nfs4
```

## **Práctica**

Si el archivo mencionado anteriormente se deja de esa forma, se crea exitosamente un homedir para el usuario que inicia sesión.

Este homedir puede ser de varios tipos de sistemas de ficheros, como por ejemplo uno temporal (**tmpfs**), y se le puede adjudicar un espacio máximo dentro del disco.

A raíz de esto se pueden hacer varios ejercicios.

### Ejercicio 1

Hacer que a todos los usuarios se les monte dentro de su home un recurso llamado "tmp" con un tamaño de 100 MB correspondiente a un ramdisk tmpfs.

- Paso 1: modificar el archivo **/etc/pam.d/common-session**.
  Al final del archivo se añade lo siguiente:

  ```
  session required	pam_unix.so
  session	required	pam_mkhomedir.so
  session	optional	pam_mount.so
  ```

- Paso 2: modificar el archivo **/etc/security/pam_mount.conf.xml**.
  Bajo la sección de **Volume definitions**:
  
  ```
                    <!-- Volume definitions -->

  volume
          user="*"                                     # Todos los usuarios.
          fstype="tmpfs"                               # El command substitution se hace con '%' en vez de con '$' en XML.
          mountpoint="/home/%(USER)/tmp"               # Se puede indicar ~/tmp.
          options="size=100M,uid=%(USER),mode=0700"    # El modo indica los permisos. Si no se añade este atributo se dan todos los permisos a todos.
  ```

### Ejercicio 2

Solo al usuario unix01 se le monta dentro de su home un recurso llamado "tmp" con un tamaño de 200 MB correspondiente a un ramdisk tmpfs.

- Paso 1: modificar el archivo **/etc/pam.d/common-session** exactamente igual
  que en el ejercicio anterior.

- Paso 2: modificar el archivo **/etc/security/pam_mount.conf.xml** igual que
  en el ejercicio anterior, pero cambiando algunos valores:

  ```
                    <!-- Volume definitions -->

  volume
          user="unix01"                                 # El usuario cambia.
          fstype="tmpfs"
          mountpoint="~/tmp"
          options="size=200M,uid=%(USER),mode=0700"     # El tamaño del dir cambia.
  ```

**IMPORTANTE**: Si se quiere definir volúmenes para todos los usuarios pero para algunos en específico otros diferentes, primero tienen que estar escritos estos últimos para que no se apliquen los generales.

- Así quedaría:

  ```
                    <!-- Volume definitions -->

  volume
          user="unix01"
          fstype="tmpfs"
          mountpoint="/home/%(USER)/tmp"
          options="size=200M,uid=%(USER),mode=0700"

  volume
          user="*"
          fstype="tmpfs"
          mountpoint="/home/%(USER)/tmp"
          options="size=100M,uid=%(USER),mode=0700"
  ```

### Ejercicio 3

Al usuario unix02 se le monta dentro de su home un recurso NFS de red. Como ejemplo se puede importar /usr/share/doc y /usr/share/man de fuera del contenedor, por lo que el host hará de servidor NFS y el servidor PAM hará de cliente NFS.

#### Servidor NFS

- Para poder exportar archivos con el servicio NFS (puerto 2049) primero hay que instalarlo en el servidor.

  ```
  apt-get install nfs-common nfs-server
  ```

- Luego hay que comprobar el estado del servicio e iniciarlo si no estaba activo.
**IMPORTANTE**: En el cliente también hay que realizar una instalación: si la distribución es Fedora, **nfs-utils**, si es Debian o Ubuntu, **nfs-common**.

  ```
  sudo systemctl status nfs-server
  ```

- Es importante desactivar el servicio cuando no se use para evitar sustos:

  ```
  sudo systemctl stop nfs-server
  sudo systemctl disable nfs-server
  sudo systemctl status nfs-server      # Siempre hay que comprobar los cambios.
  ```

- Crear el archivo **/etc/exports** y escribir lo siguiente:

  ```
  /usr/share/doc	*(ro,sync)
  /usr/share/man	*(ro,sync)
  ```

  * Esta orden muestra lo que se está exportando actualmente y tiene varias opciones, entre ellas -v (verbose) y -r (reexport).

    ```
    sudo exportfs
    ```

#### Cliente NFS

Tras haber configurado lo que se quiere exportar del servidor, hay que indicar en el cliente desde dónde se importan los archivos.

- Paso 1: modificar el archivo **/etc/pam.d/common-session** igual que en los
  ejercicios anteriores.

- Paso 2: modificar el archivo **/etc/security/pam_mount.conf.xml**.
  Bajo la sección de **Volume definitions**:
  
  ```
                    <!-- Volume definitions -->

  volume
          user="unix02"
          fstype="nfs"
          mountpoint="/home/%(USER)/"
          options="size=100M,uid=%(USER),mode=0700"
  ```

  * Igual que en el ejercicio 2, si se quiere definir volúmenes para usuarios específicos, hay que seguir un orden. **Los tres ejercicios quedarían en un solo documento así:**

    ```
                    <!-- Volume definitions -->

    <volume
            user="unix01"
            fstype="tmpfs"
            mountpoint="~/tmp"
            options="size=200M,uid=%(USER),mode=0700"/>

    <volume
            user="unix02"
            fstype="nfs"
            server="g02"
            mountpoint="~/doc"
            path="/usr/share/doc"/>

    <volume
            user="*"
            fstype="tmpfs"
            mountpoint="~/tmp"
            options="size=100M,uid=%(USER),mode=0700"/>
    ```

## **Comprobación de los ejercicios**

```
unix01@pam:~$ ls -l
total 0
drwx------ 2 unix01 unix01 40 Nov 15:59 tmp
unix01@pam:~$ df -h
...
none        100M    0  100M   0% /home/pere/tmp
none        200M    0  200M   0% /home/unix01/tmp
```