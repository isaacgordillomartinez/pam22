# PAM SERVER 2022 INTERACTIVO
# Isaac Gordillo


## **Ejecución del contenedor**

```
docker build -t isaacgm22/pam22:ldap .
```

```
docker run --rm --name pam.edt.org -h pam.edt.org --privileged --network 2hisx -it isaacgm22/pam22:ldap
```

También hará falta usar un contenedor con una base de datos LDAP.

```
docker run --rm --name ldap.edt.org -h ldap.edt.org --network 2hisx -p 389:389 -d isaacgm22/ldap22:grups
```

## **Cambios respecto al contenedor base**

### startup.sh

Se ha añadido la siguiente orden para encender los servicios **nslcd** y **nscd**.

  * **Nota**: nslcd se encarga de hacer las peticiones al servidor LDAP y nscd almacena los resultados.

    ```
    /usr/sbin/nslcd
    /usr/sbin/nscd
    ```

### Dockerfile

Se ha añadido a la orden de instalación los paquetes **libpam-ldapd**, **libnss-ldapd**, **nslcd**, **nslcd-utils** y **ldap-utils**.

Pese a que en teoría **nslcd** contiene los otros paquetes, se instalarán todos por si acaso.

```
RUN apt-get update && apt-get -y install procps tree nmap vim less finger passwd iproute2 iputils-ping libpam-pwquality libpam-mount libnss-ldapd libpam-ldapd nslcd nslcd-utils ldap-utils
```

Si no se instalasen los paquetes de forma automática gracias a este archivo, habría que instalarlos de forma manual para escoger algunas opciones, que se guardan en **/etc/nsswitch.conf**.

Sabiendo esto último, también se pueden automatizar las opciones que el usuario puede marcar durante la instalación de los paquetes copiando y sustituyendo dentro del contenedor dicho archivo. A parte, se copiarán dentro del contenedor los siguientes archivos de configuración: **/etc/ldap/ldap.conf**, **/etc/nslcd.conf**, **/etc/pam.d/common-auth**, **/etc/pam.d/common-session**, y **/etc/security/pam_mount.conf.xml**.

```
COPY ldap.conf /etc/ldap/
COPY nslcd.conf /etc/
COPY common-auth /etc/pam.d/
COPY common-session /etc/pam.d/
COPY pam_mount.conf.xml /etc/security/
COPY nsswitch.conf /etc/
```

## **Teoría**

Este es un contenedor de Docker para practicar con PAM el servicio LDAP.

Su contenido sale de este link: https://github.com/edtasixm06/pam21/tree/master/pam21:ldap.

- Paquetes necesarios para realizar la práctica:

  * **libpam-mount**. Es la librería que permite al servicio PAM montar directorios.
  * **libnss-ldapd**. Es la librería que permite al servicio LDAP actuar como un servidor de nombres.
  * **libpam-ldapd**. Es la librería necesaria para poder autenticarse como usuario LDAP dentro de un servidor PAM.
  * **ldap-utils**. Son las utilidades básicas del servicio LDAP.
  * **nslcd** (**Name Service Cache Local LDAP Daemon**). Es el demonio que permite configurar el sistema local para cargar usuarios y grupos desde un directorio LDAP.
  * **nslcd-utils**. Son las utilidades utilidades básicas del nslcd.

### Contenido de los archivos de configuración

- **/etc/ldap/ldap.conf**

  * Aquí aparecen las bases de datos con las que tiene que trabajar el servidor PAM a través del servicio LDAP ejecutado como cliente. En este caso la BD tiene como URI ldap://ldap.edt.org y como base dc=edt,dc=org.

  * **Comprobación**: ldapsearch -x -LLL. Si se muestra la base de datos del contenedor ldap.edt.org, se ha configurado exitosamente.

    ```
    #
    # LDAP Defaults
    #

    # See ldap.conf(5) for details
    # This file should be world readable but not world writable.

    BASE	dc=edt,dc=org
    URI	ldap://ldap.edt.org

    #SIZELIMIT	12
    #TIMELIMIT	15
    #DEREF		never

    # TLS certificates (needed for GnuTLS)
    TLS_CACERT	/etc/ssl/certs/ca-certificates.crt
    ```

- **/etc/nslcd.conf**

  * Este archivo define lo mismo que el anterior pero en el caché del demonio del servicio LDAP.

    ```
    # /etc/nslcd.conf
    # nslcd configuration file. See nslcd.conf(5)
    # for details.

    # The user and group nslcd should run as.
    uid nslcd
    gid nslcd

    # The location at which the LDAP server(s) should be reachable.
    uri ldap://ldap.edt.org

    # The search base that will be used for all queries.
    base dc=edt,dc=org

    # The LDAP protocol version to use.
    #ldap_version 3

    # The DN to bind with for normal lookups.
    #binddn cn=annonymous,dc=example,dc=net
    #bindpw secret

    # The DN used for password modifications by root.
    #rootpwmoddn cn=admin,dc=example,dc=com

    # SSL options
    #ssl off
    #tls_reqcert never
    tls_cacertfile /etc/ssl/certs/ca-certificates.crt

    # The search scope.
    #scope sub
    ```

- **/etc/pam.d/common-auth**

  * Aquí se configuran los pasos a seguir tras un intento de autenticación de un usuario.
  
  * Lo que hace el módulo **pam_unix.so** con la opción **nullok** es comprobar si al iniciar sesión el usuario es de tipo **unix**. Si lo es, salta dos líneas hasta llegar al módulo **pam_permit.so**

    ```
    auth	[success=2 default=ignore]	pam_unix.so nullok
    ```

    ```
    auth	required			pam_permit.so
    ```

  * Si por el contrario el usuario es de otro tipo, se ignora el resultado de la ejecución del módulo anterior y se ejecuta el siguiente módulo, **pam_ldap.so**, que comprueba si el usuario es de tipo **LDAP**. Si lo es, se ejecuta el módulo **pam_permit.so**.

    ```
    auth	[success=1 default=ignore]	pam_ldap.so minimum_uid=1000 use_first_pass
    ```

    ```
    auth	required			pam_permit.so
    ```

    Si no, se ignora el resultado y se ejecuta el módulo **pam_deny.so**.

    ```
    auth	requisite			pam_deny.so
    ```

    ```
    #
    # /etc/pam.d/common-auth - authentication settings common to all services
    #
    # This file is included from other service-specific PAM config files,
    # and should contain a list of the authentication modules that define
    # the central authentication scheme for use on the system
    # (e.g., /etc/shadow, LDAP, Kerberos, etc.).  The default is to use the
    # traditional Unix authentication mechanisms.
    #
    # As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
    # To take advantage of this, it is recommended that you configure any
    # local modules either before or after the default block, and use
    # pam-auth-update to manage selection of other modules.  See
    # pam-auth-update(8) for details.

    # here are the per-package modules (the "Primary" block)
    auth	[success=2 default=ignore]	pam_unix.so nullok
    auth	[success=1 default=ignore]	pam_ldap.so minimum_uid=1000 use_first_pass
    # here's the fallback if no module succeeds
    auth	requisite			pam_deny.so
    # prime the stack with a positive return value if there isn't one already;
    # this avoids us returning an error just because nothing sets a success code
    # since the modules above will each just jump around
    auth	required			pam_permit.so
    # and here are more per-package modules (the "Additional" block)
    auth	optional	pam_mkhomedir.so
    auth	optional	pam_mount.so 
    auth	optional	pam_cap.so 
    # end of pam-auth-update config
    ```

- **/etc/pam.d/common-session**

  * En este archivo se determina qué módulos han de ejecutarse después de que un usuario haya terminado su autenticación de forma exitosa.

    ```
    #
    # /etc/pam.d/common-session - session-related modules common to all services
    #
    # This file is included from other service-specific PAM config files,
    # and should contain a list of modules that define tasks to be performed
    # at the start and end of interactive sessions.
    #
    # As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
    # To take advantage of this, it is recommended that you configure any
    # local modules either before or after the default block, and use
    # pam-auth-update to manage selection of other modules.  See
    # pam-auth-update(8) for details.

    # here are the per-package modules (the "Primary" block)
    session	[default=1]			pam_permit.so
    # here's the fallback if no module succeeds
    session	requisite			pam_deny.so
    # prime the stack with a positive return value if there isn't one already;
    # this avoids us returning an error just because nothing sets a success code
    # since the modules above will each just jump around
    session	required			pam_permit.so
    # and here are more per-package modules (the "Additional" block)
    session	required	pam_unix.so 
    session	optional	pam_mkhomedir.so
    session	optional	pam_mount.so 
    session	optional	pam_ldap.so
    # end of pam-auth-update config
    ```

- **/etc/security/pam_mount.conf.xml**

  * La única parte que interesa entender de este archivo es la sección donde se definen con qué parámetros van a ser creados los volúmenes cuando se ejecute el módulo **pam_mkhomedir.so**.

  * La idea es crear un directorio home temporal de 100M a cada usuario de tipo LDAP cuando se autentique en este servidor PAM.

    ```
        <!-- Volume definitions -->

    <volume
      user="unix01"
      fstype="tmpfs"
      mountpoint="~/tmp"
      options="size=200M,uid=%(USER),mode=0700"/>

    <volume
      user="unix02"
      fstype="nfs"
      server="g02"
      mountpoint="~/doc"
      path="/usr/share/doc"/>

    <volume
      user="*"
      fstype="tmpfs"
      mountpoint="~/tmp"
      options="size=100M,uid=%(USER),mode=0700"/>

        <!-- pam_mount parameters: General tunables -->
    ```

- **/etc/nsswitch.conf**

  Este fichero sirve como interruptor del servicio de nombres, que escoge qué archivo de resolución de hosts se ha de usar. Por defecto primero se comprueba **/etc/hosts** y luego **/etc/resolv.conf**.

    * **Comprobación**: getent passwd y getent group. Así se verifica la resolución de los servicios que se usarán en esta práctica.

      ```
      # /etc/nsswitch.conf
      #
      # Example configuration of GNU Name Service Switch functionality.
      # If you have the `glibc-doc-reference' and `info' packages installed, try:
      # `info libc "Name Service Switch"' for information about this file.

      passwd:         files ldap      # En esta práctica solo se usarán los
      group:          files ldap      # servicios de passwd y de group.
      shadow:         files
      gshadow:        files

      hosts:          files dns
      networks:       files

      protocols:      db files
      services:       db files
      ethers:         db files
      rpc:            db files

      netgroup:       nis
      ```

## **Práctica**

El objetivo de esta práctica es montar mediante los módulos **pam_mount.so** y **pam_mkhomedir.so** entradas de directorio de forma automática para todos los usuarios de tipo LDAP que se autentiquen en el servidor PAM.

Una vez estén en marcha el contenedor de LDAP y el de PAM, hay que seguir los siguientes pasos para conseguir crear dicho directorio:

1. Comprobar la conectividad entre el servidor PAM y el servidor LDAP.
Esto se puede hacer ejecutando la siguiente orden desde dentro de este segundo contenedor:

    ```
    root@pam:/opt/docker# nmap ldap.edt.org
    ```

    ```
    PORT    STATE SERVICE
    389/tcp open  ldap
    ```

2. Como se ha comentado anteriormente, la configuración del servidor PAM se ha hecho automáticamente al copiar dentro del contenedor diversos archivos. Lo que se puede hacer en este punto es comprobar que se hayan trasladado al contenedor de forma correcta y que sus contenidos son los de la teoría.

3. Comprobar que los servicios **nslcd** y **nscd** están encendidos.

    ```
    ps ax
    ```

    ```
    PID TTY      STAT   TIME COMMAND
      1 pts/0    Ss     0:00 /bin/sh -c /opt/docker/startup.sh
      7 pts/0    S      0:00 /bin/bash /opt/docker/startup.sh
     55 pts/0    S      0:00 /bin/bash
     71 ?        Sl     0:00 /usr/sbin/nslcd
     80 pts/0    R+     0:00 ps ax
    ```

4. Iniciar sesión como usuario LDAP.

    ```
    su -l pere
    ```